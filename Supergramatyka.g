grammar Supergramatyka;

@members {
	class DivBy0Exception extends RuntimeException {
		DivBy0Exception() {
			System.out.println("Nie można dzielić przez 0.");	
		}
	}
}

plik	:
	((
		expr {System.out.println("Wynik: " + $expr.expr_res);} 
		NL
	))* 
	EOF
	;

expr returns [Integer expr_res]	:
	t1 = term {$expr_res = $t1.term_res;}
	((
		PLUS t2 = term {$expr_res += $t2.term_res;} 
		| MINUS t3 = term {$expr_res -= $t3.term_res;}
	))*
	;
	catch [DivBy0Exception foo] {$expr_res = 0;}
	
term returns [Integer term_res]	:
	t1 = term2 {$term_res = $t1.term2_res;} 
	((
		MUL t2 = term2 {$term_res *= $t2.term2_res;} 
		| DIV t3 = term2 {
			if ( $t3.term2_res != 0) {
				$term_res/= $t3.term2_res;
			} else {
				throw new DivBy0Exception();
			} 
		}
	))*
	;
	
term2 returns [Integer term2_res]	:
	a1 = atom {$term2_res = $a1.atom_res;} 
	(( MOD a2 = atom {$term2_res = $term2_res \% $a2.atom_res;} ))*
	;

atom returns [Integer atom_res]	:
	INT {$atom_res= Integer.parseInt($INT.text);} 
	| (
		LP 
		expr {$atom_res = $expr.expr_res;} 
		RP
	)
	;

ID  :
	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT :
	'0'..'9'+
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

WS  :   ( ' '
        | '\t'
        | '\r'
        ) {$channel=HIDDEN;}
    ;
    
PLUS	:	'+';
MINUS	:	'-';
MUL	:	'*';
DIV	:	'/';
MOD :	'%';

LP	:	'(';
RP	:	')';

NL	:	'\n';